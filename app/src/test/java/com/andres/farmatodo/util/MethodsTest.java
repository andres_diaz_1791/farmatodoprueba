package com.andres.farmatodo.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class MethodsTest {

    @Test
    public void isMultiple() {
        int num1 = 15;
        int num2 = 5;
        boolean expected = true;
        boolean result;

        Methods methods = new Methods();
        result = methods.isMultiple(num1, num2);

        assertEquals(result,expected);
    }

    @Test
    public void validateNumber() {
        int input = 10;
        int expected = 5;
        int result;

        Methods methods = new Methods();
        result = methods.validateNumber(input);

        assertEquals(result,expected);
    }

    @Test
    public void validateOperation() {
        String input = "6 +-(- 4)";
        boolean expected = false;
        boolean result;

        Methods methods = new Methods();
        result = methods.validateOperation(input);

        assertEquals(result,expected);
    }
}