package com.andres.farmatodo.util;

public class Constants {

    public static final String BASE_URL = "http://gateway.marvel.com/v1/public/";
    public static final String API_KEY = "6244a9e17f9746f6e431248d003839fa";
    public static final String HASH = "3f0b0bb401062d49b6a768643c36b9df";

    public static final String URL_CHARACTERS = "characters";
    public static final String URL_COMICS = "comics";
    public static final String URL_CREATORS = "creators";
    public static final String URL_EVENTS = "events";
    public static final String URL_SERIES = "series";
    public static final String URL_STORIES = "stories";

    public static final String[] URL_ARRAY = new String[]{
            URL_CHARACTERS,
            URL_COMICS,
            URL_CREATORS,
            URL_EVENTS,
            URL_SERIES,
            URL_STORIES
    };

    public static final String LOADING = "loading";
    public static final String EXHAUSTED = "exhausted";

    public static final int NETWORK_TIMEOUT = 30 * 1000;
}
