package com.andres.farmatodo.util;

public class Methods {

    private int[] intArray = new int[]{3, 5, 7, 11, 13};

    public static boolean isMultiple(int num1, int num2) {
        if (num1 % num2 == 0)
            return true;
        else
            return false;
    }

    public int validateNumber(int number) {
        for (int i = 0; i < intArray.length; i++) {
            if (isMultiple(number, intArray[i])) {
                switch (intArray[i]) {
                    case 3:
                        return 3;
                    case 5:
                        return 5;
                    case 7:
                        return 7;
                    case 11:
                        return 11;
                    case 13:
                        return 13;
                }
            }
        }
        return 0;
    }

    public boolean validateOperation(String operation) {
        StringBuilder part1 = new StringBuilder();
        StringBuilder part2 = new StringBuilder();
        String part3 = "";
        boolean isNum1Complete = false;
        boolean isValid = true;

        for (int i = 0; i < operation.length(); i++) {
            if (!isNum1Complete) {
                if (!operation.substring(i, i + 1).equalsIgnoreCase("+") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("-") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("*") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("/") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase(" ")) {
                    part1.append(operation.substring(i, i + 1));
                } else {
                    isNum1Complete = true;
                    part2.append(operation.substring(i, i + 1));
                }
            } else {
                if (!operation.substring(i, i + 1).equalsIgnoreCase("(") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("0") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("1") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("2") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("3") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("4") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("5") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("6") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("7") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("8") &&
                        !operation.substring(i, i + 1).equalsIgnoreCase("9")) {
                    part2.append(operation.substring(i, i + 1));
                } else {
                    part3 = operation.substring(i);
                    break;
                }
            }
        }
        String numero2 = part3.replace("(","").replace(")","");

        if (part2.toString().contains(" ")) {
            if (part2.toString().substring(0, 1).equalsIgnoreCase(" ") &&
                    part2.toString().substring(part2.toString().length() - 1).equalsIgnoreCase(" ")) {
                if (part2.toString().length() > 3) {
                    isValid = false;
                }
            }
            if (!part2.toString().substring(0, 1).equalsIgnoreCase(" ") &&
                    part2.toString().substring(part2.toString().length() - 1).equalsIgnoreCase(" ")) {
                isValid = false;
            }
        }

        if(numero2.substring(0,1).equalsIgnoreCase("+") ||
                numero2.substring(0,1).equalsIgnoreCase("-") ||
                numero2.substring(0,1).equalsIgnoreCase("*") ||
                numero2.substring(0,1).equalsIgnoreCase("/")){
            if (numero2.substring(1,2).equalsIgnoreCase(" ")) {
                isValid = false;
            }
        }

        return isValid;
    }
}
