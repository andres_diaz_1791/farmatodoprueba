package com.andres.farmatodo.model;

import java.util.List;

public class series {

    private Integer available;
    private String name;
    private List<items> items;

    public Integer getAvailable() {
        return available;
    }

    public String getName() {
        return name;
    }

    public List<com.andres.farmatodo.model.items> getItems() {
        return items;
    }
}
