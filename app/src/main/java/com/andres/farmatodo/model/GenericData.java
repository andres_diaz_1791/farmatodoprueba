package com.andres.farmatodo.model;

import java.util.List;

public class GenericData {
    private int total;
    private List<GenericObject> results;

    public int getTotal() {
        return total;
    }

    public List<GenericObject> getResults() {
        return results;
    }
}
