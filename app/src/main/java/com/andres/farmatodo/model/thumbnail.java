package com.andres.farmatodo.model;

public class thumbnail {

    private String path;
    private String extension;

    public String getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }
}
