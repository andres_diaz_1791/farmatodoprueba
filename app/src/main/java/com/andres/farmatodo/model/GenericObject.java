package com.andres.farmatodo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenericObject {
    private int id;
    private String title;
    private String name;
    private String fullName;
    @SerializedName("thumbnail")
    private thumbnail pics;
    private String fullPic;
    private String genericName;


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getFullname() {
        return fullName;
    }

    public thumbnail getPics() {
        return pics;
    }

    public String getFullPic() {
        return this.pics.getPath() + "." + this.pics.getExtension();
    }

    public String getGenericName() {
        if (title != null) {
            return title;
        }
        if (name != null) {
            return name;
        }
        if (fullName != null) {
            return fullName;
        }
        if (genericName != null) {
            return genericName;
        }
        return "";
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    private String description;
    private creators creators;
    private characters characters;
    private stories stories;
    private events events;
    private series series;
    private comics comics;

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public com.andres.farmatodo.model.creators getCreators() {
        return creators;
    }

    public com.andres.farmatodo.model.characters getCharacters() {
        return characters;
    }

    public com.andres.farmatodo.model.stories getStories() {
        return stories;
    }

    public com.andres.farmatodo.model.events getEvents() {
        return events;
    }

    public com.andres.farmatodo.model.series getSeries() {
        return series;
    }

    public com.andres.farmatodo.model.comics getComics() {
        return comics;
    }
}
