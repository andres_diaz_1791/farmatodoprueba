package com.andres.farmatodo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andres.farmatodo.R;
import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.util.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class MarvelListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int LIST = 1;
    private static final int LOADING_VIEW = 2;
    private static final int EXHAUSTED_VIEW = 3;


    private List<GenericObject> mlist;
    private OnMarvelItemListener onMarvelItemListener;

    public MarvelListAdapter(OnMarvelItemListener onMarvelItemListener) {
        this.onMarvelItemListener = onMarvelItemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = null;
        switch (i) {
            case LIST: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_list_item, viewGroup, false);
                return new MarvelListViewHolder(view, onMarvelItemListener);
            }
            case LOADING_VIEW: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_loading, viewGroup, false);
                return new GenericViewHolder(view);
            }
            case EXHAUSTED_VIEW: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_exhausted, viewGroup, false);
                return new GenericViewHolder(view);
            }
            default: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_list_item, viewGroup, false);
                return new MarvelListViewHolder(view, onMarvelItemListener);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        int itemViewType = getItemViewType(i);
        if (itemViewType == LIST) {
            ((MarvelListViewHolder) viewHolder).name.setText(mlist.get(i).getGenericName());
            if (mlist.get(i).getPics() != null) {
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.ic_launcher_background);
                Glide.with(viewHolder.itemView.getContext())
                        .setDefaultRequestOptions(requestOptions)
                        .load(mlist.get(i).getFullPic())
                        .into(((MarvelListViewHolder) viewHolder).image);
            } else {
                ((MarvelListViewHolder) viewHolder).image.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mlist != null) {
            return mlist.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (mlist.get(position).getGenericName().equalsIgnoreCase(Constants.LOADING)) {
            return LOADING_VIEW;
        } else if (mlist.get(position).getGenericName().equalsIgnoreCase(Constants.EXHAUSTED)) {
            return EXHAUSTED_VIEW;
        } else if (position == mlist.size() - 1
                && position != 0
                && !mlist.get(position).getGenericName().equalsIgnoreCase(Constants.EXHAUSTED)) {
            return LOADING_VIEW;
        } else {
            return LIST;
        }
    }

    public void setExhausted() {
        hideLoading();
        GenericObject genericObject = new GenericObject();
        genericObject.setGenericName(Constants.EXHAUSTED);
        mlist.add(genericObject);
        notifyDataSetChanged();
    }

    private void hideLoading() {
        if (isLoading()) {
            for (GenericObject object : mlist) {
                if (object.getGenericName().equalsIgnoreCase(Constants.LOADING)) {
                    mlist.remove(object);
                    notifyDataSetChanged();
                }
            }
        }
    }

    public void loading() {
        if (!isLoading()) {
            GenericObject genericObject = new GenericObject();
            genericObject.setGenericName(Constants.LOADING);
            List<GenericObject> newList = new ArrayList<>();
            newList.add(genericObject);
            mlist = newList;
            notifyDataSetChanged();
        }
    }

    private boolean isLoading() {
        if (mlist != null) {
            if (mlist.size() > 0) {
                if (mlist.get(mlist.size() - 1).getGenericName().equalsIgnoreCase(Constants.LOADING)) {
                    return true;
                }
            }
        }
        return false;
    }

    public GenericObject getSelectedItem(int position){
        if(mlist != null){
            if(mlist.size() > 0){
                return mlist.get(position);
            }
        }
        return null;
    }

    public void serMarvelList(List<GenericObject> mlist) {
        this.mlist = mlist;
        notifyDataSetChanged();
    }
}
