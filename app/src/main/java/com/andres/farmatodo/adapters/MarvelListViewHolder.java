package com.andres.farmatodo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.andres.farmatodo.R;

public class MarvelListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView name;
    AppCompatImageView image;
    OnMarvelItemListener onMarvelItemListener;

    public MarvelListViewHolder(@NonNull View itemView, OnMarvelItemListener onMarvelItemListener) {
        super(itemView);
        this.onMarvelItemListener = onMarvelItemListener;
        name = itemView.findViewById(R.id.name);
        image = itemView.findViewById(R.id.image);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onMarvelItemListener.onItemClick(getAdapterPosition());
    }

}
