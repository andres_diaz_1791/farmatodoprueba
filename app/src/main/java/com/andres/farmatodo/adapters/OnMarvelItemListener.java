package com.andres.farmatodo.adapters;

public interface OnMarvelItemListener {

    void onItemClick(int pos);
}
