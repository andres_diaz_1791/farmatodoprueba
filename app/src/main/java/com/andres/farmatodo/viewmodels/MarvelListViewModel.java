package com.andres.farmatodo.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.repositories.MarvelListRepository;

import java.util.List;

public class MarvelListViewModel extends ViewModel {

    private MarvelListRepository marvelListRepository;
    private boolean mIsRequesting;

    public MarvelListViewModel() {
        marvelListRepository = MarvelListRepository.getInstance();
        mIsRequesting = false;
    }

    public LiveData<List<GenericObject>> getList() {
        return marvelListRepository.getList();
    }

    public void getMarvelApi(String url){
        mIsRequesting = true;
        marvelListRepository.getMarvelApi(url);
    }
    public void getMarvelNextPageApi(String url){
        if(!isIsRequesting() &&
                !isExhausted().getValue()){
            marvelListRepository.getMarvelNextPageApi(url);
        }
    }

    public void searchMarvelApi(String url, String name) {
        mIsRequesting = true;
        marvelListRepository.searchMarvelApi(url,name);
    }

    public void searchMarvelNextPageApi(String url){
        if(!isIsRequesting() &&
        !isExhausted().getValue()){
            marvelListRepository.searchMarvelNextPageApi(url);
        }
    }

    public boolean isIsRequesting() {
        return mIsRequesting;
    }

    public void setIsRequesting(boolean mIsRequesting) {
        this.mIsRequesting = mIsRequesting;
    }

    public LiveData<Boolean> isExhausted(){
       return marvelListRepository.isExhausted();
    }

    public void cancelRequest() {
        marvelListRepository.cancelRequest();
    }
}
