package com.andres.farmatodo.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.requests.AppApiClient;

public class MarvelDetailViewModel extends ViewModel {

    private static MarvelDetailViewModel instance;
    private AppApiClient appApiClient;
    private LiveData<GenericObject> marvelItem;

    public static MarvelDetailViewModel getInstance() {
        if (instance == null) {
            instance = new MarvelDetailViewModel();
        }
        return instance;
    }

    public MarvelDetailViewModel() {
        appApiClient = AppApiClient.getInstance();
    }

    public LiveData<GenericObject> getMarvelItem() {
        return appApiClient.getItem();
    }

    public void getMarvelItemById(String url) {
        appApiClient.getMarvelItemById(url);
    }
}
