package com.andres.farmatodo.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.requests.AppApiClient;

import java.util.List;

public class MarvelListRepository {

    private static MarvelListRepository instance;
    private AppApiClient appApiClient;
    private MutableLiveData<Boolean> isExhausted = new MutableLiveData<>();
    private MediatorLiveData<List<GenericObject>> mediatorLiveData = new MediatorLiveData<>();

    private String name;
    private int offset;
    private int searchOffset;

    public static MarvelListRepository getInstance() {
        if (instance == null) {
            instance = new MarvelListRepository();
        }
        return instance;
    }

    private MarvelListRepository() {
        appApiClient = AppApiClient.getInstance();
        initMediatior();
    }

    private void initMediatior() {
        LiveData<List<GenericObject>> marvelListApiSource = appApiClient.getList();
        mediatorLiveData.addSource(marvelListApiSource, new Observer<List<GenericObject>>() {
            @Override
            public void onChanged(@Nullable List<GenericObject> genericObjects) {
                if (genericObjects != null) {
                    mediatorLiveData.setValue(genericObjects);
                    requestDone(genericObjects);
                } else {
                    requestDone(null);
                }
            }
        });
    }

    private void requestDone(List<GenericObject> list){
        if(list != null){
            if(list.size() % 20 != 0 || list.size() == 0){
                isExhausted.setValue(true);
            }
        } else {
            isExhausted.setValue(true);
        }
    }

    public LiveData<Boolean> isExhausted(){
        return isExhausted;
    }

    public LiveData<List<GenericObject>> getList() {
        return mediatorLiveData;
    }

    public void getMarvelApi(String url) {
        this.offset = 0;
        isExhausted.setValue(false);
        appApiClient.getMarvelApi(url, "0");
    }

    public void getMarvelNextPageApi(String url) {
        offset = offset + 20;
        appApiClient.getMarvelApi(url, String.valueOf(offset));
    }

    public void searchMarvelApi(String url, String name) {
        this.name = name;
        this.searchOffset = 0;
        isExhausted.setValue(false);
        appApiClient.searchMarvelApi(url, name, "0");
    }

    public void searchMarvelNextPageApi(String url) {
        searchOffset = searchOffset + 20;
        appApiClient.searchMarvelApi(url, name, String.valueOf(searchOffset));
    }

    public void cancelRequest() {
        appApiClient.cancelRequest();
    }
}
