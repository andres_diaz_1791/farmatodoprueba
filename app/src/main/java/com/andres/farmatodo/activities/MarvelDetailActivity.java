package com.andres.farmatodo.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andres.farmatodo.R;
import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.model.characters;
import com.andres.farmatodo.model.comics;
import com.andres.farmatodo.model.creators;
import com.andres.farmatodo.model.events;
import com.andres.farmatodo.model.items;
import com.andres.farmatodo.model.series;
import com.andres.farmatodo.model.stories;
import com.andres.farmatodo.viewmodels.MarvelDetailViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class MarvelDetailActivity extends AppCompatActivity {

    private LinearLayout myLinearLayout;
    private AppCompatImageView image;
    private TextView title;
    private MarvelDetailViewModel marvelDetailViewModel;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marvel_detail);

        myLinearLayout = findViewById(R.id.layout);
        image = findViewById(R.id.image);
        title = findViewById(R.id.title);

        marvelDetailViewModel = ViewModelProviders.of(this).get(MarvelDetailViewModel.class);
        getIncomingIntent();
        susbcribeObservers();
    }

    private void getIncomingIntent() {
        if (getIntent().hasExtra("url")) {
            url = getIntent().getStringExtra("url");
            marvelDetailViewModel.getMarvelItemById(url);
        }
    }

    private void susbcribeObservers() {
        marvelDetailViewModel.getMarvelItem().observe(this, new Observer<GenericObject>() {
            @Override
            public void onChanged(@Nullable GenericObject genericObject) {
                setThings(genericObject);
            }
        });
    }

    public void setThings(GenericObject genericObject){
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);
        if (genericObject.getPics() != null) {
            Glide.with(getApplicationContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(genericObject.getFullPic())
                    .into(image);
        } else {
            image.setVisibility(View.GONE);
        }

        title.setText(genericObject.getGenericName());
        setTextView(genericObject.getCharacters());
        setTextView(genericObject.getComics());
        setTextView(genericObject.getCreators());
        setTextView(genericObject.getEvents());
        setTextView(genericObject.getSeries());
        setTextView(genericObject.getStories());
    }

    public void setTextView(Object o) {
        List<items> list = null;
        if (o instanceof characters) {
            if (((characters) o).getAvailable() != 0) {
                setTitle("Characters");
                characters genericObject = (characters) o;
                list = genericObject.getItems();
            }

        }
        if (o instanceof comics) {
            if (((comics) o).getAvailable() != 0) {
                setTitle("Comics");
                comics genericObject = (comics) o;
                list = genericObject.getItems();
            }

        }
        if (o instanceof creators) {
            if (((creators) o).getAvailable() != 0) {
                setTitle("Creators");
                creators genericObject = (creators) o;
                list = genericObject.getItems();
            }
        }
        if (o instanceof events) {
            if (((events) o).getAvailable() != 0) {
                setTitle("Events");
                events genericObject = (events) o;
                list = genericObject.getItems();
            }

        }
        if (o instanceof series) {
            if (((series) o).getAvailable() == null) {
                if (((series) o).getName() != null) {
                    setTitle("Series");
                    final TextView name = new TextView(getApplicationContext());
                    name.setText("* " + ((series) o).getName());
                    myLinearLayout.addView(name);
                }
            } else {
                if (((series) o).getAvailable() != 0) {
                    setTitle("Series");
                    series genericObject = (series) o;
                    list = genericObject.getItems();
                }
            }
        }
        if (o instanceof stories) {
            if (((stories) o).getAvailable() != 0) {
                setTitle("Stories");
                stories genericObject = (stories) o;
                list = genericObject.getItems();
            }
        }

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                final TextView rowTextView = new TextView(getApplicationContext());
                rowTextView.setText("* " + list.get(i).getName());
                myLinearLayout.addView(rowTextView);
            }
        }

    }

    private void setTitle(String title){
        final TextView titleTv = new TextView(getApplicationContext());
        titleTv.setText(title);
        titleTv.setTextSize(15.0f);
        titleTv.setTextColor(Color.rgb(0, 0, 0));
        titleTv.setTypeface(Typeface.DEFAULT_BOLD);
        myLinearLayout.addView(titleTv);
    }
}
