package com.andres.farmatodo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andres.farmatodo.R;
import com.andres.farmatodo.util.Constants;
import com.andres.farmatodo.util.Methods;
import com.udojava.evalex.Expression;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    private EditText et;
    private Button button;
    private BigDecimal result;
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        methods = new Methods();
        button = findViewById(R.id.button);
        et = findViewById(R.id.et);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MarvelListActivity.class);
                try {
                    result = new Expression(et.getText().toString()).eval();
                    if (methods.validateOperation(et.getText().toString())) {
                        if (result.intValue() != 0) {
                            Toast.makeText(MainActivity.this, "Resultado " + result.intValue(), Toast.LENGTH_LONG).show();
                            switch (methods.validateNumber(result.intValue())) {
                                case 0:
                                    intent.putExtra("url", Constants.URL_STORIES);
                                    startActivity(intent);
                                    break;
                                case 3:
                                    intent.putExtra("url", Constants.URL_COMICS);
                                    startActivity(intent);
                                    break;
                                case 5:
                                    intent.putExtra("url", Constants.URL_COMICS);
                                    startActivity(intent);
                                    break;
                                case 7:
                                    intent.putExtra("url", Constants.URL_CREATORS);
                                    startActivity(intent);
                                    break;
                                case 11:
                                    intent.putExtra("url", Constants.URL_EVENTS);
                                    startActivity(intent);
                                    break;
                                case 13:
                                    intent.putExtra("url", Constants.URL_SERIES);
                                    startActivity(intent);
                                    break;
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Resultado 0", Toast.LENGTH_LONG).show();
                            intent.putExtra("url", Constants.URL_CHARACTERS);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Caso Invalido...", Toast.LENGTH_LONG).show();
                        intent.putExtra("url", Constants.URL_CHARACTERS);
                        startActivity(intent);
                    }
                } catch (Expression.ExpressionException e) {
                    intent.putExtra("url", Constants.URL_STORIES);
                    startActivity(intent);
                } catch (ArithmeticException e) {
                    intent.putExtra("url", Constants.URL_STORIES);
                    startActivity(intent);
                }
            }
        });
    }
}
