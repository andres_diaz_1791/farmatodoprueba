package com.andres.farmatodo.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import com.andres.farmatodo.R;
import com.andres.farmatodo.adapters.MarvelListAdapter;
import com.andres.farmatodo.adapters.OnMarvelItemListener;
import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.viewmodels.MarvelListViewModel;

import java.util.List;

public class MarvelListActivity extends AppCompatActivity implements OnMarvelItemListener {

    private MarvelListViewModel marvelListViewModel;
    private RecyclerView recyclerView;
    private MarvelListAdapter mAdapter;
    private SearchView searchView;
    private boolean sherchUsed = false;
    private String url;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marvel_list);
        recyclerView = findViewById(R.id.recyclerView);
        searchView = findViewById(R.id.search);
        toolbar = findViewById(R.id.toolbar);

        marvelListViewModel = ViewModelProviders.of(this).get(MarvelListViewModel.class);
        initRecyclerView();
        initSearchView();
        susbcribeObservers();
        getIncomingIntent();
    }

    private void getIncomingIntent() {
        if (getIntent().hasExtra("url")) {
            url = getIntent().getStringExtra("url");
            toolbar.setTitle(url);
            toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
            mAdapter.loading();
            getMarvelApi();
        }
    }

    private void initRecyclerView() {
        mAdapter = new MarvelListAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)) {
                    if (sherchUsed) {
                        marvelListViewModel.searchMarvelNextPageApi(url);
                    } else {
                        marvelListViewModel.getMarvelNextPageApi(url);
                    }
                }
            }
        });
    }

    private void initSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                sherchUsed = true;
                mAdapter.loading();
                marvelListViewModel.searchMarvelApi(url, query);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }


    private void susbcribeObservers() {
        marvelListViewModel.getList().observe(this, new Observer<List<GenericObject>>() {
            @Override
            public void onChanged(@Nullable List<GenericObject> genericObjects) {
                if (genericObjects != null) {
                    marvelListViewModel.setIsRequesting(false);
                    mAdapter.serMarvelList(genericObjects);
                    /*if(genericObjects.size() == 0){
                        mAdapter.setExhausted();
                    }*/
                }
            }
        });

        marvelListViewModel.isExhausted().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean) {
                    mAdapter.setExhausted();
                }
            }
        });
    }

    public void getMarvelApi() {
        marvelListViewModel.getMarvelApi(url);
    }

    @Override
    public void onItemClick(int pos) {
        GenericObject object = mAdapter.getSelectedItem(pos);
        Intent intent = new Intent(this, MarvelDetailActivity.class);
        intent.putExtra("url", url + "/" + object.getId());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        marvelListViewModel.setIsRequesting(false);
        if (sherchUsed) {
            searchView.clearFocus();
            searchView.setQuery("", false);
            mAdapter.loading();
            getMarvelApi();
            sherchUsed = false;
        } else {
            finish();
        }
    }
}
