package com.andres.farmatodo.requests;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.andres.farmatodo.model.GenericData;
import com.andres.farmatodo.model.GenericObject;
import com.andres.farmatodo.requests.responses.GenericResponse;
import com.andres.farmatodo.util.Constants;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

public class AppApiClient {


    private static AppApiClient instance;
    private MutableLiveData<List<GenericObject>> mMarvelList;
    private RetrieveMarvelRunnable retrieveMarvelRunnable;
    private RetrieveMarvelListRunnable retrieveMarvelListRunnable;
    private RetrieveMarvelItemByIdRunnable retrieveMarvelItemByIdRunnable;
    private MutableLiveData<GenericObject>mMarvelItem;

    public static AppApiClient getInstance() {
        if (instance == null) {
            instance = new AppApiClient();
        }
        return instance;
    }

    private AppApiClient() {
        mMarvelList = new MutableLiveData<>();
        mMarvelItem = new MutableLiveData<>();
    }

    public LiveData<List<GenericObject>> getList() {
        return mMarvelList;
    }

    public LiveData<GenericObject> getItem() {
        return mMarvelItem;
    }

    public void getMarvelApi(String url,String offset) {
        if (retrieveMarvelListRunnable != null) {
            retrieveMarvelListRunnable = null;
        }
        retrieveMarvelListRunnable = new RetrieveMarvelListRunnable(url, offset);
        final Future handler = AppExecutors.getInstance().networkIO().submit(retrieveMarvelListRunnable);

        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                handler.cancel(true);
            }
        }, Constants.NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private class RetrieveMarvelListRunnable implements Runnable {
        private String url;
        private String offset;
        private boolean cancelRequest;

        public RetrieveMarvelListRunnable(String url, String offset) {
            this.url = url;
            this.offset = offset;
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response response = getMarvelList(url, offset).execute();
                if (cancelRequest) {
                    return;
                }
                if (response.code() == 200) {
                    GenericData genericData = ((GenericResponse) response.body()).getData();
                    List<GenericObject> list = genericData.getResults();
                    if (offset.equalsIgnoreCase("0")) {
                        mMarvelList.postValue(list);
                    } else {
                        List<GenericObject> currentList = mMarvelList.getValue();
                        currentList.addAll(list);
                        mMarvelList.postValue(currentList);
                    }
                } else {
                    String error = response.errorBody().toString();
                    Log.e("Error", "" + error);
                    mMarvelList.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
                mMarvelList.postValue(null);
            }
        }

        private Call<GenericResponse> getMarvelList(String url,  String offset) {
            return AppApiAdapter.getAppApi().GenericCall(
                    url,
                    "1",
                    Constants.API_KEY,
                    Constants.HASH,
                    offset
            );
        }

        private void cancelRequest() {
            cancelRequest = true;
        }
    }

    public void searchMarvelApi(String url, String name, String offset) {
        if (retrieveMarvelRunnable != null) {
            retrieveMarvelRunnable = null;
        }
        retrieveMarvelRunnable = new RetrieveMarvelRunnable(url, name, offset);
        final Future handler = AppExecutors.getInstance().networkIO().submit(retrieveMarvelRunnable);

        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                handler.cancel(true);
            }
        }, Constants.NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private class RetrieveMarvelRunnable implements Runnable {
        private String url;
        private String name;
        private String offset;
        private boolean cancelRequest;

        public RetrieveMarvelRunnable(String url, String name, String offset) {
            this.url = url;
            this.name = name;
            this.offset = offset;
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response response = searchMarvel(url, name, offset).execute();
                if (cancelRequest) {
                    return;
                }
                if (response.code() == 200) {
                    GenericData genericData = ((GenericResponse) response.body()).getData();
                    List<GenericObject> list = genericData.getResults();
                    if (offset.equalsIgnoreCase("0")) {
                        mMarvelList.postValue(list);
                    } else {
                        List<GenericObject> currentList = mMarvelList.getValue();
                        currentList.addAll(list);
                        mMarvelList.postValue(currentList);
                    }
                } else {
                    String error = response.errorBody().toString();
                    Log.e("Error", "" + error);
                    mMarvelList.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
                mMarvelList.postValue(null);
            }
        }

        private Call<GenericResponse> searchMarvel(String url, String name, String offset) {
            return AppApiAdapter.getAppApi().GenericSearchCall(
                    url,
                    "1",
                    Constants.API_KEY,
                    Constants.HASH,
                    name,
                    offset
            );
        }

        private void cancelRequest() {
            cancelRequest = true;
        }
    }

    public void getMarvelItemById(String url) {
        if (retrieveMarvelItemByIdRunnable != null) {
            retrieveMarvelItemByIdRunnable = null;
        }
        retrieveMarvelItemByIdRunnable = new RetrieveMarvelItemByIdRunnable(url);
        final Future handler = AppExecutors.getInstance().networkIO().submit(retrieveMarvelItemByIdRunnable);

        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                handler.cancel(true);
            }
        }, Constants.NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private class RetrieveMarvelItemByIdRunnable implements Runnable {
        private String url;
        private boolean cancelRequest;

        public RetrieveMarvelItemByIdRunnable(String url) {
            this.url = url;
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response response = GenericCallById(url).execute();
                if (cancelRequest) {
                    return;
                }
                if (response.code() == 200) {
                    GenericData genericData = ((GenericResponse) response.body()).getData();
                    List<GenericObject> list = genericData.getResults();
                    mMarvelItem.postValue(list.get(0));
                } else {
                    String error = response.errorBody().toString();
                    Log.e("Error", "" + error);
                    mMarvelItem.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
                mMarvelItem.postValue(null);
            }
        }

        private Call<GenericResponse> GenericCallById(String url) {
            return AppApiAdapter.getAppApi().GenericCallById(
                    url,
                    "1",
                    Constants.API_KEY,
                    Constants.HASH
            );
        }

        private void cancelRequest() {
            cancelRequest = true;
        }
    }

    public void cancelRequest() {
       if(retrieveMarvelRunnable != null){
           retrieveMarvelRunnable.cancelRequest();
       }
       if(retrieveMarvelListRunnable != null){
           retrieveMarvelListRunnable.cancelRequest();
       }
    }
}
