package com.andres.farmatodo.requests;

import com.andres.farmatodo.requests.responses.GenericResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface AppApi {

    @GET()
    Call<GenericResponse> GenericCall (
            @Url String url,
            @Query("ts") String ts,
            @Query("apikey") String apikey,
            @Query("hash") String hash,
            @Query("offset") String offset
    );

    @GET()
    Call<GenericResponse> GenericSearchCall (
            @Url String url,
            @Query("ts") String ts,
            @Query("apikey") String apikey,
            @Query("hash") String hash,
            @Query("nameStartsWith") String name,
            @Query("offset") String offset
    );

    @GET()
    Call<GenericResponse> GenericCallById (
            @Url String url,
            @Query("ts") String ts,
            @Query("apikey") String apikey,
            @Query("hash") String hash
    );
}
