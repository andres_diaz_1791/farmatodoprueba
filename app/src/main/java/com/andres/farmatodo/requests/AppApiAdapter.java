package com.andres.farmatodo.requests;

import com.andres.farmatodo.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppApiAdapter {
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static AppApi appApi = retrofit.create(AppApi.class);

    public static  AppApi getAppApi(){
        return appApi;
    }
}
