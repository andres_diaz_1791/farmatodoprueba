package com.andres.farmatodo.requests.responses;

import com.andres.farmatodo.model.GenericData;

public class GenericResponse {

    private int code;
    private String status;
    private GenericData data;

    public int getCode() {
        return code;
    }
    public String getStatus() {
        return status;
    }

    public GenericData getData() {
        return data;
    }
}
